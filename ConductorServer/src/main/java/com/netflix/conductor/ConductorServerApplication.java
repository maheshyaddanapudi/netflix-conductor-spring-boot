package com.netflix.conductor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication
@EnableAutoConfiguration
public class ConductorServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConductorServerApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void startConductorServer()
	{
		
	}
}
